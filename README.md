# Documentation Example

This repository contains a small example of a documentation webpage. The goal was to make it as simple as possible with just a bit of CSS styling to make it look modern and convergent. Below are examples of how the page looks on a desktop and mobile device.

**Desktop screenshot**
![desktop screenshot](img/desktop-screenshot.png)

**Mobile screenshot**
![mobile screenshot](img/mobile-screenshot.png)
